/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 */
define(
    [
        'N/runtime', 'N/search', 'N/record', 'N/log'
    ],
    function (runtime, search, record, log) {

        function getInputData() {
            var results = search.load({id: 'customsearch_mpz_invoice_remove_lines'});
            return results;
        }

        function map(context) {
            try {

                var scriptObj = runtime.getCurrentScript();
                var searchResult = JSON.parse(context.value);
                log.audit({ title: 'Line of seaved searh', details: context.value });
                var itemInitial = scriptObj.getParameter({ name: 'custscript_mpz_gen_fac_item_initial' });
                var id = searchResult.id;
                var document_number = searchResult.values.tranid;
                log.audit({title: 'Invoice name to process',details: document_number});

                var invoice = record.load({
                    type: record.Type.INVOICE,
                    id: id,
                    isDynamic: true
                });

                var lines = invoice.getLineCount({ sublistId: 'item' });
                log.audit({title: 'Lines of the invoice', details: lines});

                for (var i = lines-1; i >=0 ;i--) {
                    invoice.selectLine({ sublistId: 'item', line: i });
                    invoice.removeLine({ sublistId: 'item', line: i, ignoreRecalc: true });
                    // log.audit({title: 'Line Removed', details: 'Line ' + i + ' was removed'});
                }

                log.audit({ title: 'Remaining item lines', details: invoice.getLineCount({ sublistId: 'item' }) });

                // invoice.setSublistValue({sublistId: 'item', fieldId: 'item', line: 0, value: itemInitial});
                // invoice.setSublistValue({sublistId: 'item', fieldId: 'quantity', line: 0, value: 1});
                // invoice.setSublistValue({sublistId: 'item', fieldId: 'amount', line: 0, value: 0});
                // invoice.setSublistValue({sublistId: 'item', fieldId: 'rate', line: 0, value: 0});
                invoice.selectNewLine({ sublistId: 'item' });
                invoice.setCurrentSublistValue({ sublistId: 'item', fieldId: 'item', value: itemInitial });
                invoice.setCurrentSublistValue({ sublistId: 'item', fieldId: 'quantity', value: 1 });
                invoice.setCurrentSublistValue({ sublistId: 'item', fieldId: 'rate', value: 0 });

                invoice.commitLine({ sublistId: 'item' });

                var invoiceID = invoice.save({ enableSourcing: true, ignoreMandatoryFields: true });
                log.debug({title: 'The invoiced has been saved', details: invoiceID});


            } catch (err) {
                log.error({title: 'Error on script execution', details: err});
            }
        }
        // function reduce(context) {
        //
        // }
        function summarize(summary) {
            log.audit('Summary Time', 'Total Seconds: ' + summary.seconds);
            log.audit('Summary Usage', 'Total Usage: ' + summary.usage);
            log.audit('Summary Yields', 'Total Yields: ' + summary.yields);

            log.audit('Input Summary: ', JSON.stringify(summary.inputSummary));
            log.audit('Map Summary: ', JSON.stringify(summary.mapSummary));
            log.audit('Reduce Summary: ', JSON.stringify(summary.reduceSummary));

            var mapSummary = summary.mapSummary;

            var errorMsg = [];
            mapSummary.errors.iterator().each(function (key, value) {
                var msg = 'Failure to accept payment from customer id: ' + key + '. Error was: ' + JSON.parse(value).message + '\n';
                errorMsg.push(msg);
                return true;
            });
            if (errorMsg.length > 0) {
                var e = error.create({
                    name: 'RECORD_TRANSFORM_FAILED',
                    message: JSON.stringify(errorMsg)
                });
            }
        }
        return {
            getInputData: getInputData,
            map: map,
            // reduce: reduce,
            summarize: summarize
        };
    });
